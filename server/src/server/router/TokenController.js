import "babel-polyfill";
import { Token } from '../models/index'
import { User } from '../models/index'
import bcrypt from  'bcrypt'
import jwt from 'jsonwebtoken'

exports.login = (req, res, next) => {
    const email = req.body.email;
    const password = req.body.password;
    let user;
    User.findOne({where: {email: email}})
        .then(newuser => {
            if (newuser) {
                user = newuser;
                return bcrypt.compareSync(password, user.password);
            } else {
                throw ({code: 400, message: "Email or password not valid"});
            }
        })
        .then(result => {
            if(result){
                Token.findAndCountAll({where:{userId: user.id}})
                    .then(result => {
                        if(result.count >= 3) {
                            Token.destroy({where:{userId: user.id}});
                        }
                        const refreshToken = jwt.sign({ role: user.role, id: user.id }, 'secret_key',{expiresIn: '30d' });
                        const accessToken = jwt.sign({ role: user.role, id: user.id }, 'secret_key',{expiresIn: '1h' });
                        Token.create({'userId' : user.id,'nameToken': refreshToken});
                        res.send({refreshToken,accessToken,user});
                    });
            }
            else {
                throw ({code: 400, message: "Email or password not valid"});
            }
        })
        .catch(err => next(err));
};
module.exports.logout = (req,res,next) =>{
    const deleteToken = req.body.refreshToken;
    Token.destroy({ where:{nameToken: deleteToken}})
        .then(result => {
            if (result > 0) {
                res.send("success");
            }
            else {
                throw ({code: 400, message: "error"})
            }
        })
        .catch(err => next(err));
};


module.exports.tokenRefresh = async (req,res,next) =>{
    const refreshToken = req.body.refreshToken;
    try {
        const result = await Token.findOne({where:{nameToken : refreshToken}});
        if(!result) throw("Refresh tokens not found");
        const decodeRefresh = jwt.verify(refreshToken,'secret_key');
        await Token.destroy({where:{nameToken: result.nameToken}});
        const user = await User.findOne({where: {id: decodeRefresh.id}});
        if(!user) throw ("user not found");
        const token = await Token.findAndCountAll({where:{userId: user.id}})
        if(token.count >= 3) {
            await Token.destroy({where:{userId: user.id}});
        }
        const newrefreshToken = jwt.sign({ role: user.role, id: user.id }, 'secret_key',{expiresIn: '30d' });
        const newaccessToken = jwt.sign({ role: user.role, id: user.id }, 'secret_key',{expiresIn: '1h' });
        await Token.create({'userId' : user.id,'nameToken': newrefreshToken});
        res.send({newrefreshToken,newaccessToken});
    } catch (e) {
        next(e);
    }
};

module.exports.authByToken = async (req, res, next) => {
    const token = req.headers.authorization.split(' ')[1];
    console.log('=================== ', token)
    try {
        const { id } = jwt.verify(token, 'secret_key');
        const newUser = await User.findById(id, {
            attributes: {
                exclude: ['password', 'createdAt', 'updatedAt']
            }
        });
        res.send(newUser);
    } catch (e) {
        next({ code: 404, message: 'not find' });
    }
};

