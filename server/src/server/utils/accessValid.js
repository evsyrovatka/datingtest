import { Token,User } from '../models/index'
import "babel-polyfill"
import jwt from "jsonwebtoken";

module.exports = async function(req,res,next ) {
    try {
        if (req.headers.authorization.split(' ')[0] !== 'Bearer' || !req.headers.authorization.split(' ')[1]) {

            throw ({message: 'Access denied'})
        }
        console.log(req.headers.authorization.split(' ')[1])
        const accessToken = req.headers.authorization.split(' ')[1];
            await jwt.verify(accessToken, 'secret_key');
        next();
    } catch (err) {
        next({status: 401});
    }
};

