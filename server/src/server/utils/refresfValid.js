import {Token, User} from "../models";
import "babel-polyfill";

const jwt = require('jsonwebtoken');

module.exports = async (req, res, next) => {
    const refresh = req.body.refresh;
    try {
        const result = await Token.findOne({where: {tokenName: refresh}});
        if (!result) throw("Refresh tokens not found");
        const decodeRefresh = jwt.verify(refresh, 'secret_key');
        await Token.destroy({where: {tokenName: result.tokenName}});
        const user = await User.findOne({where: {id: decodeRefresh.id}});
        if (!user) throw ("user not found");
        const token = await Token.findAndCountAll({where: {userId: user.id}});
        if (token.count >= 3) {
            await Token.destroy({where: {userId: user.id}});
        }
        const newrefreshToken = jwt.sign({role: user.role, id: user.id}, 'secret_key', {expiresIn: '30d'});
        const newaccessToken = jwt.sign({role: user.role, id: user.id}, 'secret_key', {expiresIn: '1h'});
        await Token.create({'userId': user.id, 'tokenName': newrefreshToken});
        res.send({newrefreshToken, newaccessToken});
    } catch (e) {
        next(e);
    }
};
