import { Token,User } from '../models/index'
import jwt from 'jsonwebtoken'
import "babel-polyfill"
module.exports = async function(req,res,next ) {
    try{
        if (!req.headers.authorization && req.headers.authorization.split(' ')[0] !== 'Bearer') {
            return next({status: 401, message: 'Access denied'})
        }
        const accessToken = req.headers.authorization.split(' ')[1];
        console.log(accessToken);
        const decode = await jwt.verify(accessToken,'secret_key');
        if(decode.role > 0) {
            next();
        }
    } catch (e) {
        return next({status: 400, message: 'no root'})
    }
};
