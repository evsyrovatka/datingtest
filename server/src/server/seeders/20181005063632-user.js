'use strict';
const bcrypt = require('bcrypt');

module.exports = {
  up: (queryInterface, Sequelize) => {
      return queryInterface.bulkInsert('Users', [{
          firstName: 'Genya',
          lastName: 'Syirovatka',
          email: 'syrokgacky@gmail.com',
          gender: 'man',
          password: bcrypt.hashSync('spartanec137gg99', bcrypt.genSaltSync(8)),
          age: 19,
          role: 2,
          isActive: true,
          createdAt: new Date(),
          updatedAt: new Date()
        },
        {
          firstName: 'Justin',
          lastName: 'Bieber',
          email: 'justinааа@gmail.com',
          gender: 'man',
          password: bcrypt.hashSync('1qaz2w3e4r', bcrypt.genSaltSync(8)),
          age: 20,
          role: 1,
          isActive: true,
          createdAt: new Date(),
          updatedAt: new Date()
        },
        {
          firstName: 'Kostya',
          lastName: 'Arabadji',
          email: 'barabarabara@gmail.com',
          gender: 'woman',
          password: bcrypt.hashSync('monster34monster', bcrypt.genSaltSync(8)),
          age: 17,
          role: 0,
          isActive: true,
          createdAt: new Date(),
          updatedAt: new Date()
        }
      ], {});
  },

  down: (queryInterface, Sequelize) => {
  }
};
