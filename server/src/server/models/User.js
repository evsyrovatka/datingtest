'use strict';
const moment = require('moment');
module.exports = (sequelize, DataTypes) => {
  const User = sequelize.define('User', {
    id: {
      allowNull: false,
      autoIncrement: true,
      primaryKey: true,
      type: DataTypes.INTEGER
    },
    firstName: {
      type: DataTypes.STRING,
      allowNull: false,
      validate: {
        notEmpty: true
      },
    },
    lastName: {
      type: DataTypes.STRING,
      allowNull: false,
      validate: {
        notEmpty: true
      }
    },
    email: {
      type: DataTypes.STRING,
      allowNull: false,
      unique: true,
      validate: {
        isEmail: true
      }
    },
    password: {
      type: DataTypes.STRING,
      allowNull: false,
      validation: {
        notEmpty: true
      }
    },
    profilePicture: {
      type: DataTypes.STRING,
      allowNull: true,
    },
    gender: {
      type: DataTypes.STRING,
      allowNull: false,
      validate: {
        notEmpty: true
      }
    },
    age: {
       type: DataTypes.INTEGER,
       allowNull: false,
      validate: {
        isAdult(value) {
          if (moment(moment().subtract(18, 'years').unix()).diff(value) < 0) {
            throw new Error('You must be over 18 years old');
          }
        }
      }

    },
    role: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0,
    },
    intention: {
      type: DataTypes.STRING,
      allowNull: false,
      defaultValue: "friend",
      validate: {
        notEmpty: true
      }
    },
    isActive: {
      type: DataTypes.BOOLEAN,
      allowNull: false,
      defaultValue: true
    },
    isBaned: {
      type: DataTypes.BOOLEAN,
      allowNull: false,
      defaultValue: false
    }
  });
  User.associate = function (models) {
      User.hasMany(models.Photo,{foreignKey: 'userId'},
          {onDelete: "CASCADE"}
      );
      User.hasMany(models.Visitor,{foreignKey: 'userId'},
          {onDelete: "CASCADE"}
      );
      User.hasMany(models.Token,{foreignKey: 'userId'},
          {onDelete: "CASCADE"}
      );
  };
  return User;
};
