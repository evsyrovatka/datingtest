 export default {
  name: 'Profile',
  props: {
    firstName: String,
    age: Number,
    email: String,
    bg: String,
    userAvatar: String
  },
  methods: {
    getAvatar () {
      if (this.userAvatar){
        return { 'background-image': `url('http://localhost:3000/api/getAvatar/${this.userAvatar}')` }
      }
      return { 'background-image': `url('http://localhost:3000/api/getAvatar/1547637731743-17241-200.png')` }

      }
    }
}
