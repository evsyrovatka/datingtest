export default {
  name: 'UserList',
  props: {
    userName: String,
    email: String,
    userAge: Number
  }
}
