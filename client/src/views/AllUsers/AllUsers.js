import {TOKENS_LOGOUT, USER_BY_ID, USERS} from '../../store/constants'
import { mapState, mapActions } from 'vuex'
import user from '../../components/user/user.vue'
export default {
  name: 'AllUsers',
  components:{
    user
  },
  created(){
    this[USERS]();
  },
  computed: {
    ...mapState({
      isFetching: state => state.user.isFetching,
      isFetch: state => state.token.isFetching,
      error: state => state.user.error,
      users: state => state.user.users,
      cu: state => state.token.currentUser
    })
  },
  methods: {
    ...mapActions([USERS]),
    async func() {
      await this[TOKENS_LOGOUT]();
      this.$router.push('/login');
      localStorage.removeItem('access');
    },
    getProfilePicture () {
      if (this.cu.profilePicture !== null){
        return `http://localhost:3000/api/getAvatar/${this.cu.profilePicture}`
      } else {
        return 'http://localhost:3000/api/getAvatar/1547628901386-17241-200.png'
      }
    },
    vis () {
      if (this.cu.role > 0){
        return { 'display': `flex`}
      }
      return { 'display': `none`}
    },
    ...mapActions([USER_BY_ID, TOKENS_LOGOUT, USERS])
  }
}

