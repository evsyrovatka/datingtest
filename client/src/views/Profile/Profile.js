import { mapActions, mapGetters, mapState } from 'vuex'

import {
  TOKENS_LOGOUT, USER_BY_ID,
  GET_USER_BY_ID,
  GET_USER_PHOTO,
  GET_USER_PHOTO_BYID,
  UPDATE_USER_AVATAR, ADD_USER_PHOTO, GET_VISITORS,
  VISIT
} from '../../store/constants'

export default {
  name: 'Profile',
   async created () {
     await this[USER_BY_ID](this.$route.params.id);
          if (this.cu.id === this.user.id) {
            await this[GET_VISITORS]()
          } else {
            await this[VISIT](this.user.id)
          }
  },
  data: function() {
    return{
      file: null
    }
  },
  computed: {
    ...mapState({
      visitors: state => state.visit.visitors,
      isFetching: state => state.user.isFetching,
      isFetch: state => state.token.isFetching,
      photos: state => state.photo.photos,
      cu: state => state.token.currentUser,
      isFet: state => state.photo.isFetching,
      fet: state => state.visit.isFetching,
      message: state => state.photo.message
    }),
    ...mapGetters([GET_USER_BY_ID, GET_USER_PHOTO_BYID]),
    user () {
      this[GET_USER_PHOTO](this.$route.params.id);
      return this[GET_USER_BY_ID](parseInt(this.$route.params.id))
    }
  },

  methods: {
    async func() {
        await this[TOKENS_LOGOUT]();
        this.$router.push('/login');
        localStorage.removeItem('access');
    },
    getPhoto(photo) {
      return `http://localhost:3000/api/getAvatar/${photo}`
    },
     bgphoto () {
      if (this.user.profilePicture){
        return { 'background-image': `url('http://localhost:3000/api/getAvatar/${this.user.profilePicture}')` }
      }
      return { 'background-image': `url('http://localhost:3000/api/getAvatar/1547637731743-17241-200.png')` }

    },
    async addPhoto() {
      if(this.file === null)
        alert("please choose file");

      else {
        await this[ADD_USER_PHOTO]({photoName: this.file, id: this.cu.id});
        if (this.message)
          await this[GET_USER_PHOTO](this.$route.params.id);
        this.file = null
      }
    },

    getProfilePicture () {
      if (this.cu.profilePicture !== null){
        return `http://localhost:3000/api/getAvatar/${this.cu.profilePicture}`
      } else {
        return 'http://localhost:3000/api/getAvatar/1547628901386-17241-200.png'
      }
    },
    async updateAvatar() {
      if(this.file === null)
        alert("please choose file");
      else {
        await this[UPDATE_USER_AVATAR]({profilePicture: this.file, id: this.cu.id});
        location.reload();
        this.file = null
      }
    },


    ...mapActions([USER_BY_ID, TOKENS_LOGOUT, GET_USER_PHOTO, UPDATE_USER_AVATAR, ADD_USER_PHOTO,GET_VISITORS, VISIT])
  }
}
