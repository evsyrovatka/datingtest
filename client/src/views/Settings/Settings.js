import {USER_BAN, GET_USERS_BAN, USER_ACTIVE} from '../../store/constants'
import { mapState, mapActions } from 'vuex'

export default {
  name: 'Settings',

  created(){
    this[GET_USERS_BAN]();
  },
  computed: {
    ...mapState({
      isFetching: state => state.user.isFetching,
      isFetch: state => state.token.isFetching,
      error: state => state.user.error,
      users: state => state.user.users,
      cu: state => state.token.currentUser
    })
  },
  methods: {
    async banUser(id, isBaned, role) {
      await this[USER_BAN]({id: id, isBaned: isBaned, role: role});
    },
    async deactiveUser(id, isActive, role) {
      await this[USER_ACTIVE]({id: id, isActive: isActive, role: role});
    },
    ...mapActions([USER_BAN, GET_USERS_BAN, USER_ACTIVE])
  }
}
