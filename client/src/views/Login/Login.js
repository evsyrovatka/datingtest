import {mapState, mapActions} from 'vuex'
import {CREATE_USER, TOKENS} from '../../store/constants'
import TextInput from '../../components/TextInput/TextInput.vue'
export default  {
  name: 'Login',
  components:{
    TextInput
  },
  data: function () {
    return {
      Email: 'syrokgacky@gmail.com',
      password: 'spartanec137gg99',
      firstName: '',
      lastName: '',
      age: null,
      gender: '',
      passwordCreate: '',
      emailCreate: '',
      checked: false
    }
  },
  computed: {
    ...mapState({
      user: state => state.token.user,
      isFetching: state => state.token.isFetching,
      error: state => state.token.error,
      message: state =>state.user.message
    })
  },

  methods: {
    async func() {
      await this[TOKENS]({email: this.Email, password: this.password});
      if(localStorage.access) {
        this.$router.push('/AllUsers');
      }
    },
    async create() {
      if(this.checked){
      await this[CREATE_USER]({email: this.emailCreate,
        password: this.passwordCreate,
        age: this.age,
        gender: this.gender,
        firstName: this.firstName,
        lastName: this.lastName
        });
        if(this.message) {
          await this[TOKENS]({email: this.emailCreate, password: this.passwordCreate});
            this.$router.push('/AllUsers');
        }
      }
    },
    changeMethod: function (fieldValue, fieldName) {
      this[fieldName] = fieldValue
    },

    ...mapActions([TOKENS, CREATE_USER])
  }
}

