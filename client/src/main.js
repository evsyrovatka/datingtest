import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store/index.js'
import axios from 'axios'
import { restURL } from './api/baseURL'

Vue.config.productionTip = false;


axios.interceptors.response.use((response) => {
  return response
}, function (error) {
  let originalRequest = error.config;
  if (error.response.status === 401 && !originalRequest._retry) {
    originalRequest._retry = true;
    const refreshToken = localStorage.getItem('refresh');
    return axios.post(restURL + '/refresh', {refreshToken: refreshToken})
      .then(({data}) => {

        localStorage.setItem('access', data.newaccessToken);
        localStorage.setItem('refresh', data.newrefreshToken);

        originalRequest.headers['Authorization'] = 'Bearer ' + data.newaccessToken;
        return axios(originalRequest)
      }).catch(() => {
        localStorage.removeItem('access');
        localStorage.removeItem('refresh');
        router.push('/login');
      })
  }

  if (error.response.status === 405 && !originalRequest._retry) {
    originalRequest._retry = true;
    window.location.href = '/login';
    return
  }

  // Do something with response error
  return Promise.reject(error)
});

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app');
