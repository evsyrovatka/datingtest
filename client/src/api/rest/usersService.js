import { restURL } from '../baseURL'
import axios from 'axios'

export const getAllUsers = () => axios.get(restURL + '/AllUsers', {headers: { Authorization: `Bearer ${localStorage.getItem('access')} `}});

export const getUserById = (uid) => axios.get(restURL + '/profile/' + uid, {headers: { Authorization: `Bearer ${localStorage.getItem('access')} `}});

export const createUser = (createData) => axios.post(restURL + '/user', createData);

export const banUser = (user) => axios.put(restURL + '/user/ban/' + user.id, user, {headers: { Authorization: `Bearer ${localStorage.getItem('access')} `}});

export const getAllUsersBan = () => axios.get(restURL + '/ban', {headers: { Authorization: `Bearer ${localStorage.getItem('access')} `}});

export const deactiveUser = (user) => axios.put(restURL + '/user/active/' + user.id, user,  {headers: { Authorization: `Bearer ${localStorage.getItem('access')} `}});

export const updateAvatar = (data) => {
  const formData = new FormData;
  formData.append('file', data.profilePicture);
  return axios.post(restURL + '/avatar/' + data.id, formData, {headers: {Authorization: `Bearer ${localStorage.getItem('access')} `, 'Content-Type': 'multipart/form-data' }})
};
