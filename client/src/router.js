import Vue from 'vue'
import Router from 'vue-router'
import Login from './views/Login/Login.vue'
import AllUsers from './views/AllUsers/AllUsers.vue'
import Profile from './views/Profile/Profile.vue'
import Settings from './views/Settings/Settings.vue'
import store from './store/index'
import {GET_CURRENT_USER, AUTH_USER} from "./store/constants";

Vue.use(Router);

const router = new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/login',
      name: 'login',
      component: Login
    },
    {
      path: '/AllUsers',
      name: 'AllUsers',
      component: AllUsers,
      meta: {requiresAuth : true}
    },
    {
      path: '/profile/:id',
      name: 'profile',
      component: Profile,
      meta: {requiresAuth : true}
    },
    {
      path: '/settings',
      name: 'settings',
      component: Settings,
      meta: {requiresAuth : true}
    },
  ]
})

const authCurrentUserWatcher = new Promise((resolve, reject) => {
  store.watch(() => store.getters[GET_CURRENT_USER].currentUser, newState => {
    newState.error ? reject(newState.error) : resolve(newState.currentUser)
  })
});

router.beforeEach((to, from, next) => {
  if (to.matched.some(record => record.meta.requiresAuth)) {
    if (store.state.token.currentUser === null) {
      authCurrentUserWatcher
        .then(() => next())
        .catch(() => next({
          path: '/login',
          query: {
            redirect: to.fullPath
          } }));
      store.dispatch(AUTH_USER)
    } else {
      next()
    }
  }
  next()
});

export default router;
