import Vue from 'vue'
import Vuex from 'vuex'
import user from './modules/user'
import photo from './modules/photo.js'
import token from './modules/token.js'
import visit from './modules/visit.js'

Vue.use(Vuex);

export default new Vuex.Store({
  modules: {
    user,
    photo,
    token,
    visit
  }
})
