import {GET_VISITORS, VISIT, VISIT_ERROR, VISIT_REQUEST, VISIT_RESPONSE, VISITER_RESPONSE} from '../constants'

import {getVisitors, visitUser} from '../../api/rest/visitService'

export default ({
  state: {
    message: null,
    users: [],
    isFetching: false,
    error: null
  },

  mutations: {
    [VISIT_REQUEST](state) {
      state.isFetching = true
    },
    [VISIT_RESPONSE](state, visitors) {
      state.visitors = visitors;
      state.isFetching = false;
      state.error = null
    },
    [VISITER_RESPONSE](state) {
      state.isFetching = false;
      state.error = null
    },
    [VISIT_ERROR](state, error) {
      state.error = error;
      state.isFetching = false
    }
  },

  actions: {
    async [VISIT]({commit}, id) {
      commit(VISIT_REQUEST);
      try {
        const {data} = await visitUser(id);
        commit(VISITER_RESPONSE, data)
      } catch (e) {
        commit(VISIT_ERROR, e)
      }
    },
    async [GET_VISITORS]({commit}) {
      commit(VISIT_REQUEST);
      try {
        const {data} = await getVisitors();
        commit(VISIT_RESPONSE, data)
      } catch (e) {
        commit(VISIT_ERROR, e)
      }
    }
  },

})
