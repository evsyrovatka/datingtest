import {authUser, logIn, logout} from '../../api/rest/tokenService.js'
import {
  AUTH_USER,
  CURRENT_USER,
  CURRENT_USER_ERROR,
  GET_CURRENT_USER,
  TOKENS,
  TOKENS_ERROR,
  TOKENS_LOGOUT,
  TOKENS_LOGOUT_RESPONSE,
  TOKENS_REQUEST,
  TOKENS_RESPONSE
} from '../constants.js'

export default ({
  state: {
    currentUser: null,
    user: null,
    isFetching: false,
    error: null
  },
  mutations: {
    [TOKENS_REQUEST](state) {
      state.isFetching = true
    },
    [TOKENS_RESPONSE](state, data) {
      localStorage.setItem('access', data.accessToken);
      localStorage.setItem('refresh', data.refreshToken);
      state.user = data.user;
      state.isFetching = false;
      state.error = null
    },
    [TOKENS_ERROR](state, error) {
      state.error = error;
      state.isFetching = false
    },

    [TOKENS_LOGOUT_RESPONSE](state) {
      state.error = null;
      state.isFetching = false;
      state.currentUser = null;
      localStorage.removeItem('access');
      localStorage.removeItem('refresh');
    },
    [CURRENT_USER](state, currentUser) {
      state.currentUser = currentUser;
      state.isFetching = false;
      state.error = null
    },

    [CURRENT_USER_ERROR](state, error) {
      state.error = error;
      state.isFetching = false;
    }
  },
  actions: {
    async [TOKENS]({commit}, loginData) {
      commit(TOKENS_REQUEST);
      try {
        const {data} = await logIn(loginData);
        commit(TOKENS_RESPONSE, data)
      } catch (e) {
        commit(TOKENS_ERROR, e)
      }
    },

    async [AUTH_USER]({commit}) {
      commit(TOKENS_REQUEST);
      try {
        const {data} = await authUser();
        commit(CURRENT_USER, data)
      } catch (error) {
        commit(CURRENT_USER_ERROR, error.response.data)
      }
    },

    async [TOKENS_LOGOUT]({commit}) {
      commit(TOKENS_REQUEST);
      try {
        await logout();
        commit(TOKENS_LOGOUT_RESPONSE)
      } catch (e) {
        commit(TOKENS_ERROR, e)
      }
    }
  },
  getters: {
    [GET_CURRENT_USER]: state => ({
      error: state.error,
      currentUser: state.currentUser
    })
  }
})
